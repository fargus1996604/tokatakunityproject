﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour {

    public float ForceMultiplier = 3.0f;
    Rigidbody2D rigidbody;

    public int hitCount;

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody2D>();
        hitCount = PlayerPrefs.GetInt("score",0);
	}
	
	// Update is called once per frame
	void Update () {


         if (Input.GetButton("Fire1")) {      

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);   
            Vector2 rayPossition = new Vector2(ray.origin.x, ray.origin.y);
            Vector2 ballPossition = new Vector2(transform.position.x, transform.position.y);

                
            //get direction from ball possition to touch possition
            Vector2 direction = (rayPossition - ballPossition).normalized;
            //apply force multiplier
            direction.x *= ForceMultiplier;
            //ignore y axis
            direction.y = 0;
            //add force to ball
            rigidbody.AddForce(direction, ForceMode2D.Force);
            
            
           
            

        }
        //Save hit count on back pressed
        if (Input.GetKeyUp(KeyCode.Escape))
        {     
            PlayerPrefs.SetInt("score", hitCount);
            SceneManager.LoadScene(0);
        }


    }

    //Jump counter
    private void OnCollisionEnter2D(Collision2D collision)
    {
        hitCount++;
    }


}
