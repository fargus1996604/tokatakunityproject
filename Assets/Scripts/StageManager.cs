﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour {

    public Camera camera;
	public static int _StageType=0;

	public Color lightBlueColor;
	public Color grayColor;
	public Color orangeColor;

	// Use this for initialization
	void Start () {
		switch (_StageType) {
		case 0:
			camera.backgroundColor = lightBlueColor;
			Physics2D.gravity = new Vector2 (0,-9.8f);
			break;

		case 1:
			camera.backgroundColor = grayColor;
			Physics2D.gravity = new Vector2 (0,-1.6f);
			break;

		case 2: 
			camera.backgroundColor = orangeColor;
			Physics2D.gravity = new Vector2 (0,-24.5f);
			break;

		default:
			camera.backgroundColor = lightBlueColor;
			Physics2D.gravity = new Vector2 (0,-9.8f);
			break;
		}

	}
	


}
