﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MenuManager : MonoBehaviour {

	public Text scoreBoard;

	// Use this for initialization
	void Start () {

		int score = PlayerPrefs.GetInt ("score", 0);
		scoreBoard.text = "BallHit : " + score;
		
	}




	public void ChangeLevel(int type){
		StageManager._StageType = type;
		SceneManager.LoadScene (1);
	}
}
