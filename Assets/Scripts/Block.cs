﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class Block : MonoBehaviour,IPointerClickHandler
{

    SpriteRenderer sprite;

    private void Start()
    {
        sprite = GetComponent <SpriteRenderer> ();
    }

   
    //Change color on collided with ball
    private void OnCollisionEnter2D(Collision2D collision)
    {
        ChangeColor();
    }


    public void ChangeColor(){
        sprite.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
    }

    

    public void OnPointerClick(PointerEventData eventData)
    {

        Debug.Log("123123");
        ChangeColor();
    }
}
